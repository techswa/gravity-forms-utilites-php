<?php

/*
Plugin Name: SWA EYCS Gravity Forms
Version: 1.0.0
Plugin URI:
Author: Smarter Web Apps
Author URI: http://www.smarterwebapps.com/
*/


// [sponsor_advertisement type="primary" location="dashboard" location="sidebar"]

include_once('includes/constants.php');

include_once('includes/shortcodes-filters-actions.php');


// //Queries
include_once('includes/queries/swa-populate-states-and-abbreviations.php');
include_once('includes/queries/swa-populate-mbpro-sponsors.php');

//
// //Views
// include_once("includes/views/advertisement-views.php");
// include_once("includes/views/bb-callout-view.php");

//Controllers
include_once('includes/controllers/swa-create-composite-user-name.php');
include_once('includes/controllers/swa-create-mortgage-pro-membership-records.php');
include_once('includes/controllers/swa-create-real-estate-pro-membership-records.php');

include_once('includes/controllers/swa-gf-user-login-after-register.php');
include_once('includes/controllers/swa-create-affiliate-sponsor-record.php');
include_once('includes/controllers/swa-create-affiliate-subscriber-record.php');
include_once('includes/controllers/swa-create-emember-sponsor-record.php');
include_once('includes/controllers/swa-create-emember-subscriber-record.php');

include_once('includes/controllers/swa-set-members-home-directory.php');
include_once('includes/controllers/swa-populate-email-address.php');
include_once('includes/controllers/swa-populate-license-number.php');
include_once('includes/controllers/swa-populate-user-name.php');
include_once('includes/controllers/swa-populate-license-type.php');
include_once('includes/controllers/swa-populate-license-state.php');
include_once('includes/controllers/swa-gf-set-image-headshot-upload-path.php');
// include_once('includes/controllers/swa-gf-rename-image-headshot-file.php');
include_once('includes/controllers/swa-gf-resize-current-users-headshot.php');
include_once('includes/controllers/swa-gf-display-current-users-headshot.php');



//Process new subscribers
include_once('includes/controllers/swa-gf-update-affiliate-id-field.php');
include_once('includes/controllers/swa-create-subscriber-silver-membership-records.php');

// include_once("includes/controllers/swa-eycs-sponsor-advertisements.php");
// include_once("includes/controllers/swa-eycs-advertisement-sidebar.php");
// include_once("includes/controllers/swa-eycs-convert-raw-sponsor.php");
// //Javascript
// include_once("includes/js/enqueue-js-scripts.php");

// re_agent
// re_broker
// nmls
//
// firm_postition / broker - agent - etc
