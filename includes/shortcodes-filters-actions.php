<?php


// ***** MORTGAGE PRO CREATION ***** //
add_filter('gform_pre_submission_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_eycs_create_composite_user_name');
// add_action('gform_user_registered_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_login_new_user', 10, 3);
add_action('gform_after_submission_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_eycs_after_submission_mortgage_pro_handler', 10, 2);
add_action('gform_after_submission_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_set_members_home_directory', 10, 2);

// ***** REAL ESTATE PRO CREATION ***** //
add_filter('gform_pre_submission_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_eycs_create_composite_user_name');
// add_action('gform_user_registered_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_login_new_user', 10, 3);
add_action('gform_after_submission_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_eycs_after_submission_real_estate_pro_handler', 10, 2);
add_action('gform_after_submission_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_set_members_home_directory', 10, 2);

// ***** PROFESSIONAL MARKETING ***** //
//Display the image when the form is shown
add_filter('gform_pre_render_' . GF_PROFESSIONAL_SPONSOR_EDIT_ID, 'swa_gf_display_current_users_headshot');
add_filter('gform_admin_pre_render_' . GF_PROFESSIONAL_SPONSOR_EDIT_ID, 'swa_gf_display_current_users_headshot');
// - Only required when sponsor has edited their profile
add_filter('gform_after_submission_' . GF_PROFESSIONAL_SPONSOR_EDIT_ID, 'swa_gf_resize_current_users_headshot');
add_filter('gform_admin_after_submission_' . GF_PROFESSIONAL_SPONSOR_EDIT_ID, 'swa_gf_resize_current_users_headshot');

//Rename the file stored in the upload field before uploading
// add_action('gform_pre_submission_' . GF_PROFESSIONAL_SPONSOR_EDIT_ID, 'swa_gf_rename_image_headshot_file');
//Set headshot upload path
add_filter('gform_upload_path_' . GF_PROFESSIONAL_SPONSOR_EDIT_ID, 'swa_gf_set_image_headshot_upload_path');

//Load read only form data
add_filter('gform_field_value_email_address', 'swa_populate_email_address');
add_filter('gform_field_value_license_number', 'swa_populate_license_number');
add_filter('gform_field_value_license_state', 'swa_populate_license_state');
add_filter('gform_field_value_license_type', 'swa_populate_license_type');
add_filter('gform_field_value_user_name', 'swa_populate_user_name');

//Process new subcribers
//Update the affiliate_id field prior to saving
// add_action('gform_pre_submission_' . GF_CONSUMER_CREATE_ID, 'swa_eycs_update_affiliate_id_field');
// add_action('gform_user_registered_' . GF_CONSUMER_CREATE_ID, 'swa_login_new_user', 10, 3);
// add_action('gform_user_registered', 'bs_test', 10, 4);
// add_action( 'gform_after_submission_' . GF_CONSUMER_CREATE_ID, 'swa_eycs_after_submission_subscriber_silver_handler', 10, 2 );
add_action( 'gform_after_submission_'. GF_CONSUMER_CREATE_ID, 'swa_eycs_create_subscriber_silver_handler', 10, 2 );
