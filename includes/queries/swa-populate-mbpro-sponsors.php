<?php

/**
 * Return a list of states and their abbreviations
 *
 * @param
 * @return    void
 * @author
 * @copyright
 */

add_filter( 'gform_pre_render_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_populate_mbpro_sponsors' );
add_filter( 'gform_pre_validation_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_populate_mbpro_sponsors' );
add_filter( 'gform_pre_submission_filter_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_populate_mbpro_sponsors' );
add_filter( 'gform_admin_pre_render_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_populate_mbpro_sponsors' );


 function swa_populate_mbpro_sponsors( $form ){
// https://www.gravityhelp.com/documentation/article/dynamically-populating-drop-down-fields/

 	foreach( $form['fields'] as &$field ) {

  if ( $field->type != 'select' || strpos( $field->cssClass, 'mb-sponsors' ) === false ) {
          continue;
      }
      //Query users for mb_pro sponsors
      $sponsors = swa_get_mb_sponsors();

      // allocate arrary to hold selections
      $choices = array();

      //Create index for array creation
      $index = 0;
      foreach ( $sponsors as $sponsor ){

          // Create string to pass
         $user_login = $sponsor->user_login;

         // Get metadata and build text string for display
         $selection_text = swa_get_mb_sponsor_details($sponsor->ID, $user_login);

         //Build the select list
         $choices[$index]['text'] = $selection_text;
         $choices[$index]['value'] = $sponsor->user_login;

         //increment index
         $index++;
      }

      //Load selections into the listbox
      $field->choices = $choices;

     return $form;
   }
}

function swa_get_mb_sponsor_details($sponsor_id, $user_login){

  $name_last = get_user_meta($sponsor_id, $key = USER_META_NAME_LAST, true);
  $name_first = get_user_meta($sponsor_id, $key = USER_META_NAME_FIRST, true);
  $firm_name = get_user_meta($sponsor_id, $key = USER_META_FIRM_NAME, true);

  //Build display string
  $selection_text = $user_login . ' : ' . $name_last . ', ' . $name_first . ' : ' . $firm_name;

  return $selection_text;

}

// Return all mortgage sponsors
function swa_get_mb_sponsors(){
//http://code.tutsplus.com/tutorials/mastering-wp_user_query--cms-23204

  $args = array(
      // 'fields' => 'user_login',
      'role' => 'mb_pro'
  );

  // Custom query.
  $user_query = new WP_User_Query( $args );

  // Get query results.
  $sponsors = $user_query->get_results();

  return $sponsors;
}
