<?php

/**
 * Return a list of states and their abbreviations
 *
 * @param
 * @return    void
 * @author
 * @copyright
 */

 // Implement pre-render, pre-validation, pre-submission filter and admin pre-render hooks
add_filter( 'gform_pre_render_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_populate_states_and_abbreviations' );
add_filter( 'gform_pre_validation_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_populate_states_and_abbreviations' );
add_filter( 'gform_pre_submission_filter_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_populate_states_and_abbreviations' );
add_filter( 'gform_admin_pre_render_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_populate_states_and_abbreviations' );

// Run for RE professional creationadd_filter( 'gform_pre_render_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_populate_states_and_abbreviations' );
add_filter( 'gform_pre_render_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_populate_states_and_abbreviations' );
add_filter( 'gform_pre_validation_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_populate_states_and_abbreviations' );
add_filter( 'gform_pre_submission_filter_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_populate_states_and_abbreviations' );
add_filter( 'gform_admin_pre_render_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_populate_states_and_abbreviations' );


 function swa_populate_states_and_abbreviations( $form ){
// https://www.gravityhelp.com/documentation/article/dynamically-populating-drop-down-fields/

  //  $registered_classes = array( 'us-states' );
 	foreach( $form['fields'] as &$field ) {

  if ( $field->type != 'select' || strpos( $field->cssClass, 'us-states' ) === false ) {
          continue;
      }

   $choices[] = array(
     'text'  => 'Texas',
     'value' => 'TX',
   );

        // update 'Select a Post' to whatever you'd like the instructive option to be
        // $field->placeholder = 'Select a Post';
        $field->choices = $choices;

     return $form;
   }
}
// https://www.gravityhelp.com/documentation/article/dynamically-populating-drop-down-fields/

//http://zaroutski.com/populating-gravity-forms-dropdown-field-options-dynamically/
  //  $registered_classes = array( 'us-states' );
 // 	foreach( $form['fields'] as &$field ) {
 // 		// Get array of all CSS classes of the field
 // 		$field_classes = explode( ' ', $field['cssClass'] );
 // 		// Find a match in CSS class setting
 // 		$matched_css_classes = array_intersect( $registered_classes, $field_classes );
 // 		// Ignore this hook if not a drop down with the appropriate class
 // 		if( $field['type'] != 'select' || !empty( $matched_css_classes ) === FALSE ) {
 // 			continue;
 // 		}
  //
  //  $choices[] = array(
  //    'text'  => 'Texas',
  //    'value' => 'TX',
  //  );
  //        $field['choices'] = $choices;
 //
 //     return $form;
 // }
