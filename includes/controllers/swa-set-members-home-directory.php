<?php


/**
 *
 * Create a new affilaite record for the user the gravity forms has just saved.
 * (affilaite_id, first_name, last_name, email_address, date_joined, account_status, country, commission_level)
 * Note: the css class "swa-eycs-new-affiliate" has been placed on the user_login field.
 * user_login has been created prior to user being created.
 *
 * @param     $entry, $form
 * @return    void
 * @author
 * @copyright
 */

function swa_set_members_home_directory( $entry, $form ){

 //Get the data that was used to create the user record
 //rgar() is a gravity forms function that parses the $entry(['key']) returning value

   // Get the user_name
   $user_name = rgar( $entry, '7' );

   //Get user ID
   $new_user = get_user_by('login',$user_name);
   PC::debug($new_user);
   //Get id from object
   $new_user_id = $new_user->ID;
   PC::debug($new_user->ID);

  //  $user = get_user_by( 'email', 'user@example.com' );
  //  echo 'User is ' . $user->first_name . ' ' . $user->last_name;

   //Get the state of the new sponsor
   $state = $_POST['input_2'];

   //Get the type of sponsor
   $license_type = $_POST['input_9'];

   //Create composite memebr home directory
   $home_directory = "{$state}/{$license_type}/{$user_name}";

   // Add the home directory to user meta
   add_user_meta($new_user_id, USER_META_HOME_DIRECTORY, $home_directory);

}
