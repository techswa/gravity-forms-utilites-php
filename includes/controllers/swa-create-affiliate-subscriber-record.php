<?php


/**
 *
 * Create a new affilaite record for subscriber.
 * (affilaite_id, first_name, last_name, email_address, date_joined, account_status, country, commission_level)
 * Note: the css class "swa-eycs-new-affiliate" has been placed on the user_login field.
 * user_login has been created prior to user being created.
 *
 * @param     $entry, $form
 * @return    void
 * @author
 * @copyright
 */

function swa_create_affiliate_subscriber_record( $entry, $form, $membership_level ){
 global $wpdb;


   //Set referrer_id = EYCS if creating a new MB
   if ($membership_level == MEMBERSHIP_LEVEL_MORTGAGE_PRO){
     $referrer = HOUSE_ACCOUNT;

   } else {
     //Primary sponsor
     $referrer = rgar( $entry, '14' );
   }

 $signup_date = date("Y-m-d"); //yyyy-mm-dd - matches wp_affiliate
 $account_status = 'approved';
 $password_text = rgar( $entry, '3' );
 $password_hash = swa_wp_hash_password( $password_text );


 //Get the data that was used to create the user record
 //rgar() is a gravity forms function that parses the $entry(['key']) returning value

   $refid = rgar( $entry, '13' ); //user_name
   $password = $password_hash;
   $firstname = rgar( $entry, '4.3' );
   $lastname = rgar( $entry, '4.6' );
   $email = rgar( $entry, '2' );
   $date = $signup_date;
   $commission_level = '1';
   $account_status = $account_status;

   $affiliates_table_name = $wpdb->prefix . SWA_WP_AFF_AFFILIATES_TBL_NAME;

   // Create new affiliate record
   $sql = "INSERT INTO $affiliates_table_name ";
   $sql .= "(refid, pass, firstname, lastname, email, date, commissionlevel, referrer, account_status) ";
   $sql .= "VALUES ('$refid', '$password', '$firstname', '$lastname', '$email', '$date', '$commission_level', '$referrer', '$account_status')";

   //Insert new record
   $results = $wpdb->query($sql);

   if ( $results ) {
     return $results;
   } else {
     return false;
   }
}
