<?php

/**
 * Resize the sponsor image after one has been uploaded.
 *
 * @param
 * @return    void
 * @author
 * @copyright
 */

// //Display the image when the form is shown
//* active * add_filter( 'gform_admin_pre_render_' . GF_PROFESSIONAL_MARKETING_ID, 'swa_gf_resize_current_users_headshot' );

 function swa_gf_resize_current_users_headshot(){

   // get the current user information
   $current_user = wp_get_current_user();

   //Get user id
   $user_id = $current_user->ID;

  // Read image path from the user's meta record
  $image_file_url = get_user_meta($user_id, USER_META_IMAGE_HEADSHOT, 'single');

  //Break out if there isn't an image file
if (!file_exists($image_file_url)){
  return; //Return to caller
}

  // get the current user information
  $current_user = wp_get_current_user();

  // Current user ID
  $current_user_id = $current_user->ID;

  // Create object to process with and load the image file
  $image = new Imagick($image_file_url);

  // Set the target dimensions
  $targetWidth = 100;
  $targetHeight = 100;

  //Resize image file
  // $image->thumbnailImage($targetWidth, $targetHeight);
  $image->scaleImage($targetWidth, $targetHeight);

  // Get the users path & url for their home directories
  $home_directory_info = swa_get_current_users_home_directory_info( $current_user );

  //Get the users $home_directory_path
  $home_directory_path = $home_directory_info['path'];

  //Parse file name from url - filename.ext
  $filepath = basename($image_file_url);

  // Get the file info - basename, filename, ext
  $fileinfo = pathinfo($filepath);

  // If the output file name is a jpeg then change to jpg
  if ($fileinfo['extension'] == 'jpeg') {
    $basename = $fileinfo['filename'] . '.jpg';
  }

  //Build up output file path and names
  $output_file = $home_directory_path . 'images/' . $basename;

// Add code to delete any existig files

  //Write the changed file back to the users directory
  file_put_contents ($output_file, $image);

  //Release object
  $image->clear();

  return;
 }
