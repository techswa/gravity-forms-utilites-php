<?php


/**
 *
 * Create a new emember record for the user the gravity forms has just saved.
 * (affilaite_id, first_name, last_name, email_address, date_joined, account_status, country, commission_level)
 * Note: the css class "swa-eycs-new-affiliate" has been placed on the user_login field.
 * user_login has been created prior to user being created.
 *
 * @param     $entry, $form
 * @return    void
 * @author
 * @copyright
 */

// Only called on form #3 - Mortgage Professional
 // * active * add_action( 'gform_after_submission_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_eycs_after_submission_real_estate_pro_handler', 10, 2 );

function swa_eycs_after_submission_real_estate_pro_handler($entry, $form) {

  $membership_level = MEMBERSHIP_LEVEL_REAL_ESTATE_PRO;

  //make copies of data - easier than returning from create_affiliate()
  $ememeber_entry = $entry;
  $ememeber_form = $form;

  // Create affilate record for the mortgage professional
  $result = swa_create_affiliate_sponsor_record( $entry, $form, $membership_level );

  // Create emember record for the mortgage professional
  $result = swa_create_emember_sponsor_record( $ememeber_entry, $ememeber_form, $membership_level );

//Rollback new user upon issue???

}
