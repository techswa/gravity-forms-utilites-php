<?php

/**
 * Load the user_name
 *
 * @param
 * @return    string $user_name
 * @author
 * @copyright
 */

// * active * add_filter('gform_field_value_user_name', 'swa_populate_user_name');

// This is used so that field is read only
function swa_populate_user_name(){

  $current_user = wp_get_current_user();

  $user_name = $current_user->user_login;

  return $user_name;
}
