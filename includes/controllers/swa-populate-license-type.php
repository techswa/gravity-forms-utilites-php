<?php

/**
 * Load the user's license type (RE,MB)
 *
 * @param
 * @return    void string
 * @author
 * @copyright
 */

// * active * add_filter('gform_field_value_license_type', 'swa_populate_license_type');

// This is used so that field is read only
function swa_populate_license_type(){

    $current_user = wp_get_current_user();

    $current_user_id = $current_user->ID;

    $license_type = get_user_meta($current_user_id, $key = USER_META_LICENSE_TYPE, true);
    if($license_type == 'RE_BROKER'){
      $license_type = 'BROKER';
    }
    return $license_type;
}

// function get_user_meta($user_id, $key = '', $single = false) {
// 	return get_metadata('user', $user_id, $key, $single);
// }
