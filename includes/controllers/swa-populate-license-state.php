<?php

/**
 * Load the user's license staet of issuance
 *
 * @param
 * @return    void string
 * @author
 * @copyright
 */

// * active * add_filter('gform_field_value_license_state', 'swa_populate_license_state');

// This is used so that field is read only
function swa_populate_license_state(){

    $current_user = wp_get_current_user();

    $current_user_id = $current_user->ID;

    $license_state = get_user_meta($current_user_id, $key = USER_META_LICENSE_STATE, true);

    return $license_state;
}

// function get_user_meta($user_id, $key = '', $single = false) {
// 	return get_metadata('user', $user_id, $key, $single);
// }
