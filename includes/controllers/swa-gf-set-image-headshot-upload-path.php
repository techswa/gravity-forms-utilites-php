<?php

/**
 * Change the file upload path to the user's base path
 * wp-content/uploads/{state}/{license_type}/{user_login}/
 * @param
 * @return    void
 * @author
 * @copyright
 */

//*active* add_filter('gform_upload_path_' . GF_PROFESSIONAL_MARKETING_ID, "swa_gf_set_image_headshot_upload_path");

function swa_gf_set_image_headshot_upload_path( $path_info ){

// get the current user information
$current_user = wp_get_current_user();

// Current user ID
$current_user_id = $current_user->ID;


// Get the users path & url for their home directories
$home_directory_info = swa_get_current_users_home_directory_info( $current_user );

// PC::debug('Set upload path');
// Set vars for GF to save the file
$path_info['path'] = $home_directory_info['path'] . IMAGE_DIRECTORY . '/';
$path_info['url'] = $home_directory_info['url'] . IMAGE_DIRECTORY . '/';

return $path_info;

}
