<?php

/**
 *
 * Create composite user_name
 * Constructs the composite user_name from the state, advertiser_type, last_initial, license# and first_initial
 *
 * @param gravity form
 * @return    void Update $_POST['input_7'] with composite user_name
 * @author
 * @copyright
 */

// Run for MB sponsor creation
// * active * add_filter( 'gform_pre_submission_' . GF_MB_PROFESSIONAL_CREATE_ID, 'swa_eycs_create_composite_user_name' );
// Run for RE sponsor creation
// * active * add_filter( 'gform_pre_submission_' . GF_RE_PROFESSIONAL_CREATE_ID, 'swa_eycs_create_composite_user_name' );

function swa_eycs_create_composite_user_name( $form ){

  $state = $_POST['input_2'];
  $license_number = $_POST['input_4'];
  $license_type = $_POST['input_9'];
  $first_name = $_POST['input_3_3'];
  $last_name = $_POST['input_3_6'];

  $user_name = $state;
  $user_name .= $license_type;
  //Get first initial
  $user_name .= $last_name[0];
  $user_name .= $license_number;
  //Get first initial
  $user_name .= $first_name[0];

  //Replace hidden user_name with correct data
  $_POST['input_7'] = strtoupper($user_name);



}
