<?php

/**
 * Display the current users headshot in the marketing form
 *
 * @param
 * @return    void
 * @author
 * @copyright
 */

// //Display the image when the form is shown
//* active * add_filter( 'gform_pre_render_' . GF_PROFESSIONAL_MARKETING_ID, 'swa_gf_display_current_users_headshot' );
//* active * add_filter( 'gform_admin_pre_render_' . GF_PROFESSIONAL_MARKETING_ID, 'swa_gf_display_current_users_headshot' );

 function swa_gf_display_current_users_headshot( $form ){

   // get the current user information
   $current_user = wp_get_current_user();

   //Get user id
   $user_id = $current_user->ID;

  // Read image path from the user's meta record
  $image_handle = get_user_meta($user_id, USER_META_IMAGE_HEADSHOT, 'single');

  //get the field to display the image in
  $image_display_field = GFFormsModel::get_field( $form, 24 );

//get the html content
$image_display_field['content'] = "<div><img class='photo img-circle' src='{$image_handle}'></div>";


return $form;
 }
