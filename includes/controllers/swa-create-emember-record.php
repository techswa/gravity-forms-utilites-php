<?php


/**
 *
 * Create a new emember record for the user the gravity forms has just saved.
 * (affilaite_id, first_name, last_name, email_address, date_joined, account_status, country, commission_level)
 * Note: the css class "swa-eycs-new-affiliate" has been placed on the user_login field.
 * user_login has been created prior to user being created.
 *
 * @param     $entry, $form
 * @return    void
 * @author
 * @copyright
 */

// Only called on form #3 - Mortgage Professional
 // add_action( 'gform_after_submission_3', 'swa_eycs_after_submission_mortgage_pro_handler', 10, 2 );

function swa_create_emember_record($entry, $form, $membership_level) {

    global $wpdb;





    $password_text = rgar( $entry, '10' );
    $password_hash = swa_wp_hash_password( $password_text );

    // If the referrer is empty than tie to house account_state
    if (isset($_COOKIE['ap_id'])){
      $referrer = $_COOKIE['ap_id'];
    } else {
      $referrer = HOUSE_ACCOUNT;
    };

    //Get the data that was used to create the user record
    //rgar() is a gravity forms function that parses the $entry(['key']) returning value
      $user_name = rgar( $entry, '7' ); //user_name
      $first_name = rgar( $entry, '3.3' );
      $last_name = rgar( $entry, '3.6' );
      $password = $password_hash;
      $member_since = date("Y-m-d"); //yyyy-mm-dd - matches wp_affiliate
      $membership_level = $membership_level;
      $account_state = 'active';
      $email = rgar( $entry, '8' );
      $referrer = $_COOKIE['ap_id'];
      $subscription_starts = $member_since; //first subscription date
      $initial_membership_level = $membership_level;



      $emember_table_name = $wpdb->prefix . SWA_WP_EMEMBER_MEMBERS_TABLE_NAME;

      // Create new affiliate record
      $sql = "INSERT INTO $emember_table_name ";
      $sql .= "(user_name, first_name, last_name, password, member_since, membership_level, account_state, email, referrer, subscription_starts, initial_membership_level ) ";
      $sql .= "VALUES ('$user_name', '$first_name', '$last_name', '$password', '$member_since', '$membership_level', '$account_state', '$email', '$referrer', '$subscription_starts', '$initial_membership_level')";

      //Insert new record
      $results = $wpdb->query($sql);

      if ( $results ) {
        return $results;
      } else {
        return false;
      }

}
